# Bank Pro

## Deskripsi
Aplikasi bank pro adalah aplikasi web bank sederhana yang dibangun dengan menggunakan 
ReactJs. Pada aplikasi ini, pengguna dapat melakukan transaksi di Bank Pro. Pengguna yang 
dapat menggunakan aplikasi adalah nasabah Bank Pro. Berikut adalah hal yang dapat dilakukan oleh pengguna pada aplikasi Bank Pro.
    1. Login
        Nasabah login dengan memasukkan bank nomor rekening. Jika nasabah berhasil masuk, akan muncul halaman utama yang menampilkan nama pemilik, 
        nomor rekening, nama bank, dan saldo terakhir. Pada menu utama, pengguna juga dapat memilih untuk transfer ke rekening lain dan melihat riwayat transaksi.
    2. Melakukan Transfer
        Pengguna dapat mentransfer uang ke rekening lain di Bank Pro. Tidak ada potongan tambahan ketika mentransfer ke rekening bank lain. Saat transaksi selesai, ada pesan berhasil atau gagal yang muncul
    3. Melihat Riwayat Transaksi
        Pada riwayat transaksi, pengguna dapat melihat daftar data berikut: waktu transaksi, jenis transaksi (debit/kredit), jumlah transaksi, dan rekening terkait (jika ada).

## Screenshot Aplikasi
1. Login
![](public/images/login.png)

2. Home-Transfer
![](public/images/home-transfer.png)

3. Home-Transfer-Success
![](public/images/home-transfer-success.png)

4. Home-Transfer-Fail
![](public/images/home-transfer-fail.png)

5. Home-History-Transaction
![](public/images/home-history.png)

## Pembagian Kerja
- Login: 13517075
- Halaman utama: 13517075
- Transafer: 13517057
- Riwayat transfer: 13517078
- CI/CD: 13517057 13517075
- Eksplorasi dan setup deployment: 13517057 13517075
- Unit Testing: 13517057

## URL Deployment
http://ec2-54-227-190-218.compute-1.amazonaws.com:3000/