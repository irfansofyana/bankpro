import React, { useState } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import PrivateRoute from './PrivateRoute';
import Home from './routes/Home';
import Login from './routes/Login';
import Logout from './routes/Logout';
import { AuthContext } from "./contexts/auth";

function App(props) {
  const tokens = localStorage.getItem('tokens');
  const [rekeningUser, setRekeningUser] = useState(tokens);
  const [authTokens, setAuthTokens] = useState(tokens);
  
  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  }

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Router>
        <PrivateRoute 
          exact path="/" 
          component={() => <Home rekeningUser={rekeningUser} />}
        />
        <Route 
          exact path="/login" 
          render={(props) => <Login {...props} setRekeningUser={setRekeningUser} />}
        />
        <PrivateRoute 
          exact path="/logout" 
          component={() => <Logout/>}
        />
      </Router>
    </AuthContext.Provider>
  );
}

export default App;