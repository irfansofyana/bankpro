import React from "react";

const TransactionIcon = (props) => {
  const { active, setActive } = props
  
  const Image = (props) => {
    if (props.active) {
      return <img className="icon" alt="" src="/images/transaction-white.png" />
    } else {
      return <img className="icon" alt="" src="/images/transaction-black.png" />
    }
  }

  return(
    <div>
      <div className="icon-title text-center">Transfer</div>
      <div 
        className={active ? "icon-wrapper active" : "icon-wrapper"}
        onClick={setActive}
      >
        <Image className = "transaction-image" active={active}/>
      </div>
    </div>
  )
}

export default TransactionIcon;