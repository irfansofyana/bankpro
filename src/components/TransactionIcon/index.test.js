import React from "react";
import { mount } from "enzyme";

import TransactionIcon from "./index";

let transactionIcon;

beforeEach(() => {
  transactionIcon = mount(<TransactionIcon />);
});

afterEach(() => {
  transactionIcon.unmount();
});

it('Renders three <TransactionIcon /> components', () => {
  const wrapper = mount(<TransactionIcon className = "icon-title text-center" />);
  expect(wrapper.text()).toEqual("Transfer");
});

it("TransactionIcon has one image", () => {
  const wrapper = mount(<TransactionIcon className = "transaction-image" />);
  expect(wrapper.length).toEqual(1);
});

it("TransactionIcon has three div", () => {
  expect(transactionIcon.find("div").length).toEqual(3);
});
 