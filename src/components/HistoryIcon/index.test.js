import React from "react";
import { mount } from "enzyme";

import HistoryIcon from "./index";

let historyIcon;

beforeEach(() => {
  historyIcon = mount(<HistoryIcon />);
});

afterEach(() => {
  historyIcon.unmount();
});

it('Renders three <HistoryIcon /> components', () => {
  const wrapper = mount(<HistoryIcon className = "icon-title text-center" />);
  expect(wrapper.text()).toEqual("History");
});

it("HistoryIcon has one image", () => {
  const wrapper = mount(<HistoryIcon className = "history-image" />);
  expect(wrapper.length).toEqual(1);
});

it("HistoryIcon has three div", () => {
  expect(historyIcon.find("div").length).toEqual(3);
});
 