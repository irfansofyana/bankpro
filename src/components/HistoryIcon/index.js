import React from "react";

const HistoryIcon = (props) => {
  const { active, setActive } = props
  
  const Image = (props) => {
    if (props.active) {
      return <img className="icon" alt="" src="/images/history-white.png" />
    } else {
      return <img className="icon" alt="" src="/images/history-black.png" />
    }
  }

  return(
    <div>
      <div className="icon-title text-center">History</div>
      <div 
        className={active ? "icon-wrapper active" : "icon-wrapper"}
        onClick={setActive}
      >
        <Image className="history-image" active={active}/>
      </div>
    </div>
  )
}

export default HistoryIcon;