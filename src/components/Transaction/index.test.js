import React from "react";
import { mount } from "enzyme";

import Transaction from "./index";

let transaction;

beforeEach(() => {
  transaction = mount(<Transaction />);
});

afterEach(() => {
  transaction.unmount();
});

it("Transaction has one div", () => {
	const wrapper = mount(<Transaction className = "login mx-auto" />);
	expect(wrapper.length).toEqual(1);
});

it("Transaction has one button", () => {
  expect(transaction.find("button").length).toEqual(1);
});

it("Transaction has one h4", () => {
  expect(transaction.find("h4").length).toEqual(1);
});

it("Transaction has one form", () => {
	expect(transaction.find("form").length).toEqual(1);
});

it("Transaction has one label", () => {
  expect(transaction.find("label").length).toEqual(2);
});

it("Transaction has one p", () => {
  expect(transaction.find("p").length).toEqual(1);
});