import React, { useState } from "react";
import { 
  Form, 
  FormGroup, 
  Label, 
  Input, 
  InputGroup, 
  InputGroupAddon, 
  InputGroupText, 
  Button 
} from 'reactstrap';
import { options } from '../../config'

const axios = require('axios');
const parser = require('fast-xml-parser');
const Swal = require('sweetalert2');

const Transaction = (props) => {
  const { rekeningUser } = props;
  const [rekening, setRekening] = useState('');
  const [nominal, setNominal] = useState(0);  
  const [errorMessage, setErrorMessage] = useState('');
  const [errorRekening, setErrorRekening] = useState(false)

  const handleRekeningChange = (props) => {
    setRekening(props);
    if (!(props.match(/^[0-9]{12}$/))) {
      setErrorMessage('Rekening tidak valid!');
      setErrorRekening(true);
    } else {
      setErrorRekening(false);
    }
  }

  const handleNominalChange = (props) => {
    setNominal(props);
  }

  const postTransfer = () => {
    if (!errorRekening) {

      var sr = `
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rek="http://rekening.server/">
           <soapenv:Header/>
           <soapenv:Body>
              <rek:transaction>
                 <pengirim>${rekeningUser}</pengirim>
                 <penerima>${rekening}</penerima>
                 <nominal>${nominal}</nominal>
              </rek:transaction>
           </soapenv:Body>
        </soapenv:Envelope>
      `;

      axios.post('http://ec2-18-212-188-140.compute-1.amazonaws.com:8080/web/DoTransaksi', sr, { 
        headers: {'Content-Type': 'text/xml'}
      }).then(res => {
        const xml = res.data;
        if (parser.validate(xml) === true) {
          var jsonObj = parser.parse(xml,options);
          var result = jsonObj["S:Envelope"]["S:Body"]["ns2:transactionResponse"]["return"];

          if (result) {
            Swal.fire({
              title: 'Success',
              text: 'Transfer berhasil !',
              icon: 'success',
              confirmButtonText: 'Close'
            }).then(() => {
              window.location.reload();
            });
          } else {
            Swal.fire({
              title: 'Failed',
              text: 'Pastikan rekening valid dan saldo mencukupi',
              icon: 'error',
              confirmButtonText: 'Close'
            }).then(() => {
              window.location.reload();
            })
          }
        }
      }).catch(err => {
        console.log(err)
        Swal.fire({
          title: 'Failed',
          text: 'Pastikan rekening valid dan saldo mencukupi',
          icon: 'error',
          confirmButtonText: 'Close'
        }).then(() => {
          window.location.reload();
        })
      });
    }    
  }

  return(
    <div className="login mx-auto">
      <h4 className="text-center font-weight-bold">Transfer - BankPro</h4>
      <Form className="mt-5">
        <FormGroup>
          <Label>No. Rekening Tujuan</Label>
          <Input
            bsSize="sm"
            type="text"
            name="tujuan"
            id="tujuan"
            placeholder="xxxx xxxx xxxx xxxx"
            onChange={(e) => handleRekeningChange(e.target.value)}
          />
          <p className="text-error" hidden={!errorRekening}>{errorMessage}</p>
          <br/>
          <Label>Nominal</Label>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>Rp</InputGroupText>
            </InputGroupAddon>
            <Input
              bsSize="sm"
              type="text"
              name="nominal"
              id="nominal"
              onChange={(e) => handleNominalChange(e.target.value)}
            />
          </InputGroup>
          <Button 
            outline 
            color="info" 
            size="sm" 
            className="submit-btn mt-3 mx-auto" 
            block
            onClick={() => postTransfer()}
          >
            Transfer
          </Button>
        </FormGroup>
      </Form>
    </div>
  )
}

export default Transaction;