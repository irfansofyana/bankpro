import React from "react";
import { mount } from "enzyme";

import History from "./index";

let history;

beforeEach(() => {
  history = mount(<History />);
});

afterEach(() => {
  history.unmount();
});

it("History has one table", () => {
  expect(history.find("table").length).toEqual(1);
});

it("History has one tr", () => {
  expect(history.find("tr").length).toEqual(1);
});

it("History has one tbody", () => {
  expect(history.find("tbody").length).toEqual(1);
});

it("History has four th", () => {
  expect(history.find("th").length).toEqual(4);
});

