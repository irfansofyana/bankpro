import React, {useState, useEffect} from "react";
import { Table, Badge } from 'reactstrap';

const History = (props) => {
	const [transaction, setTransaction] = useState([]);
	
	useEffect(() => {
		setTransaction(props.transaksi);
	}, [props.transaksi]);
	
  const moneyNormalize = (s) => { 
    return s.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); 
  }

  const transactionWrapper = transaction.map((val, index) => {
    var date = new Date(val.waktu+'Z');

    return (   
      <tr>
        <td> {date.toDateString()} </td>
        <td> 
          <Badge color={val.jenis === 'KREDIT' ? "success" : "danger"}> </Badge> 
          {val.jenis}
        </td>
        <td> Rp {moneyNormalize(val.nominal)} </td>
        <td> {val.tujuan} </td>
      </tr>
    );
  });
  
	return (
    <Table hover size="sm">
      <thead>
        <tr>
          <th>Transaction Date</th>
          <th>Transaction Type</th>
          <th>Amount</th>
          <th>Related Account</th>
        </tr>
      </thead>
      <tbody>
        {transactionWrapper}
      </tbody>
    </Table>
	)
}

export default History;