import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { useAuth } from "../contexts/auth";
import { options } from '../config'

const axios = require('axios');
const parser = require('fast-xml-parser');

const Login = (props) => {
  const { setAuthTokens } = useAuth();
  const { setRekeningUser } = props;

  const [rekening, setRekening] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [errorRekening, setErrorRekening] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  
  const handleChange = (props) => {
    setRekening(props);
    if (!(props.match(/^[0-9]{12}$/))) {
      setErrorMessage('Rekening tidak valid!');
      setErrorRekening(true);
    } else {
      setErrorRekening(false);
    }
  }

  const postLogin = () => {
    if (!errorRekening) {

      var sr = `
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rek="http://rekening.server/">
        <soapenv:Header/>
        <soapenv:Body>
          <rek:validate>
            <nomor_rekening>${rekening}</nomor_rekening>
          </rek:validate>
        </soapenv:Body>
      </soapenv:Envelope>
      `;
      
      axios.post('http://ec2-18-212-188-140.compute-1.amazonaws.com:8080/web/ValidateRekening', sr, { 
        headers: {'Content-Type': 'text/xml'}
      }).then(res => {
        const xml = res.data;

        if (parser.validate(xml) === true) {
          var jsonObj = parser.parse(xml,options);
          if (jsonObj["S:Envelope"]["S:Body"]["ns2:validateResponse"]["return"] !== -1) {
            setIsLogin(true);
            setRekeningUser(rekening);
            setAuthTokens(rekening);
          } else {
            setErrorMessage('Rekening tidak sesuai!');
            setErrorRekening(true);
          }
        }
      }).catch(err => {
        console.log(err)
      });
    }
  }
  
  if (isLogin || localStorage.getItem('tokens')) {
    return <Redirect to="/" />;
  }

  return (
    <div className="login mx-auto">
      <h4 className="text-center font-weight-bold">Sign In - BankPro</h4>
      <Form className="mt-5">
        <FormGroup>
          <Label>No. Rekening</Label>
          <Input
            bsSize="sm"
            type="text"
            name="rekening"
            id="rekening"
            placeholder="xxxx xxxx xxxx xxxx"
            value={rekening}
            onChange={(e) => handleChange(e.target.value)}
          />
          <p className="text-error" hidden={!errorRekening}>{errorMessage}</p>
          <Button 
            outline 
            color="info" 
            size="sm" 
            className="submit-btn mt-3 mx-auto" 
            block
            disabled={errorRekening && isLogin}
            onClick={() => postLogin()}
          >
            Submit
          </Button>
        </FormGroup>
      </Form>
    </div>
  );
}

export default Login;