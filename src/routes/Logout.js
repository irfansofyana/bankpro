import React from "react";
import { Redirect } from "react-router-dom";
import { useAuth } from "../contexts/auth";

const Logout = (props) => {
  const { setAuthTokens } = useAuth();
  setAuthTokens("");
  localStorage.clear("tokens");
  
  return <Redirect to="/login/" />;
}

export default Logout;