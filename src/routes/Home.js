import React, { useState, useEffect } from "react";
import { Row, Col, Button } from 'reactstrap';
import TransactionIcon from '../components/TransactionIcon';
import HistoryIcon from '../components/HistoryIcon';
import Transaction from '../components/Transaction';
import History from '../components/History';
import { options } from '../config'
import { Link } from "react-router-dom";

const axios = require('axios');
const parser = require('fast-xml-parser');

function Home(props) {
  const { rekeningUser } = props;
  
  const [transactionActive, setTransactionActive] = useState(true);
  const [historyActive, setHistoryActive] = useState(false);
  const [nama, setNama] = useState("");
  const [nomor, setNomor] = useState("");
  const [saldo, setSaldo] = useState(0);
  const [transaksi, setTransaksi] = useState([]);

  const moneyNormalize = (s) => { 
    return s.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); 
  }

  const activateHistory = () => {
    setTransactionActive(false);
    setHistoryActive(true);
  }

  const activateTransaction = () => {
    setTransactionActive(true);
    setHistoryActive(false);
  }

  const Content = (props) => {
    if (transactionActive) {
      return <Transaction rekeningUser={rekeningUser}/>
    } else {
      return <History transaksi={props.transaksi}/>
    }
  }

  var sr = `
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rek="http://rekening.server/">
    <soapenv:Header/>
    <soapenv:Body>
      <rek:detail>
        <nomor_rekening>${rekeningUser}</nomor_rekening>
      </rek:detail>
    </soapenv:Body>
  </soapenv:Envelope>
  `;

  useEffect(() => {
    axios.post('http://ec2-18-212-188-140.compute-1.amazonaws.com:8080/web/DetailRekening', sr, { 
      headers: {'Content-Type': 'text/xml'}
    }).then(res => {
      const xml = res.data;
      
      if (parser.validate(xml) === true) {
        var jsonObj = parser.parse(xml,options);
        var result = jsonObj["S:Envelope"]["S:Body"]["ns2:detailResponse"]["return"];
        setNama(result["nama"]);
        setNomor(result["nomor_rekening"]);
        setSaldo(result["saldo"]);
        setTransaksi(result["transaksi"]);
      }
    }).catch(err => {
      console.log(err);
    });
  }, [sr]);

  return (
    <Row>
      <Col md="3" className="left-navbar">
        <div className="header">
          <div className="avatar-wrapper mx-auto mb-2">
            <img className="avatar" alt="" src="https://corporate.valentlau.com.au/img/michael-pro-016e.jpg" />
          </div>
          <div className="user-name">{nama}</div>
          <div className="user-rekening">{nomor}</div>
          <Link to="/logout">
            <Button color="secondary" size="sm" className="mt-2">Logout</Button>
          </Link>
        </div>
        <div className="action-group mt-3">
          <TransactionIcon active={transactionActive} setActive={activateTransaction}/>
          <HistoryIcon active={historyActive} setActive={activateHistory}/>
        </div>
        <div className="balance-card mx-auto mb-4">
          <div className="balance-text text-center">Current Balance</div>
          <div className="balance-nominal text-center">Rp {moneyNormalize(saldo)}</div>
        </div>
      </Col>
      <Col md="9" className="right-content">
        <Content transaksi={transaksi}/>
      </Col>
    </Row>
  );
}

export default Home;